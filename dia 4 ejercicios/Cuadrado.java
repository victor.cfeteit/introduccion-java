public class Cuadrado extends Figura{

    
    public int lado;
    



    public Cuadrado(int alto, int ancho, int lado) {
        super(alto, ancho);
        this.lado = lado;
    }


    
    @Override
    public double calcularArea() {
        double resultado = lado * lado;
        return resultado;
    }
}
