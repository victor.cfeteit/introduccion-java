public class Triangulo extends Figura {

    public int trianlados;


    public Triangulo(int alto, int ancho, int trianlados) {
        super(alto, ancho);
        this.trianlados = trianlados;

    }

    @Override
    public double calcularArea() {
        int resultadot = ( trianlados* trianlados) / 2;
        return resultadot;
    }

    
}
