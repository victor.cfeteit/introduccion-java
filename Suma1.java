import java.util.Scanner; // el programa usa la clase Scanner

public class Suma1
{

    public static void main( String args[]) // el método main empieza la ejecución de la aplicación en Java
    {
        Scanner ingresa = new Scanner( System.in ); // se crea objeto Scanner para obtener la entrada de la ventana de comandos

        float numx;
        float numz;
        float sumak;

        System.out.print("Ingrese dos números reales: ");
        numx = ingresa.nextFloat();
        numz = ingresa.nextFloat();

        sumak = numx + numz;
        


        System.out.printf("El resultado del número es: %.3f", sumak);
    }



}